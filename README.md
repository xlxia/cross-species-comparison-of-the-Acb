# Cross-Species Comparison of the Acb

We provide pairs of connectionally comparable prefrontal target regions between humans and macaques. These targets were delineated based on the voxel-level similarity of the resting-state functional connectivity fingerprints.